import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:mvp/bloc/flow_bloc.dart';
import 'package:mvp/utils/constants.dart';
import 'package:mvp/model/data_model.dart';

class TopBar extends StatelessWidget {
  final CardType activeType;
  final bool isApproval;
  final int approvalsTotal;
  final int assignmentsTotal;

  TopBar({
    @required this.activeType,
    @required this.approvalsTotal,
    @required this.assignmentsTotal,
  }) : isApproval = activeType == CardType.Approval;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(21.0, 12.0, 21.0, 12.0),
      child: Stack(
        children: <Widget>[
          AnimatedAlign(
            curve: Curves.easeOut,
            duration: Duration(milliseconds: !isApproval ? 0 : 0),
            alignment: isApproval ? Alignment.bottomCenter : Alignment.bottomLeft,
            child: AnimatedOpacity(
              child: GestureDetector(
                onTap: () {
                  BlocProvider.of<FlowBloc>(context).add(ShowApprovals());
                },
                child: SummaryInfo(
                  title: 'Pending Approvals',
                  itemsCount: approvalsTotal,
                  highlightedText: '3 new',
                  isApproval: true,
                  isActive: isApproval,
                ),
              ),
              duration: Duration(milliseconds: 800),
              opacity: isApproval ? 1.0 : 0.5,
            ),
          ),
          AnimatedAlign(
            curve: Curves.easeOut,
            duration: Duration(milliseconds: isApproval ? 0 : 0),
            alignment: !isApproval ? Alignment.bottomCenter : Alignment.bottomRight,
            child: AnimatedOpacity(
              child: GestureDetector(
                onTap: () {
                  BlocProvider.of<FlowBloc>(context).add(ShowAssignments());
                },
                child: SummaryInfo(
                  title: 'Assignments',
                  itemsCount: assignmentsTotal,
                  highlightedText: '2 past due',
                  isApproval: false,
                  isActive: !isApproval,
                ),
              ),
              duration: Duration(milliseconds: 800),
              opacity: !isApproval ? 1.0 : 0.5,
            ),
          ),
        ],
      ),
    );
  }
}

class SummaryInfo extends StatelessWidget {
  final String title;
  final int itemsCount;
  final String highlightedText; // TODO: refactor make separate widget
  final bool isApproval; // TODO: make separate classes for Approvals and Assignments
  final bool isActive;

  SummaryInfo({
    this.title,
    this.itemsCount,
    this.highlightedText,
    @required this.isActive,
    @required this.isApproval,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            title,
            style: kTopLabelTextStyle.copyWith(fontSize: isActive ? 18.0 : 14.0),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: isActive ? 10.0 : 6.0),
          Text(
            itemsCount.toString(),
            style: kCountTextStyle.copyWith(fontSize: isActive ? 76.0 : 36.0),
            textAlign: TextAlign.center,
          ),
          Text(
            highlightedText,
            style: kBottomTextStyle.copyWith(fontSize: isActive ? 16.0 : 14.0).copyWith(
                color: isActive
                    ? isApproval ? kBottomTextStyle.color : Color(0xffff0404)
                    : Colors.white), // TODO: refactor
            textAlign: TextAlign.center,
          ),
          Opacity(
            opacity: isApproval ? 1.0 : 0.0,
            child: Visibility(
              visible: isActive,
              child: Column(
                children: <Widget>[
                  SizedBox(height: 7.0),
                  Text(
                    'since Monday',
                    style: kTopLabelTextStyle.copyWith(fontSize: 16.0),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
