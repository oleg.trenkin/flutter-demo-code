import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:mvp/model/data_model.dart';
import 'package:mvp/bloc/flow_bloc.dart';
import 'package:mvp/utils/constants.dart';
import 'package:mvp/screens/details.dart';

class Card extends StatelessWidget {
  final CardData cardData;

  Card(this.cardData);

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Column(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(6)),
              boxShadow: [
                BoxShadow(
                  color: const Color(0x31000000),
                  offset: Offset(0, 2),
                  blurRadius: 2,
                  spreadRadius: 0,
                ),
              ],
              color: const Color(0xfff4f8fb),
            ),
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        CardBanner(cardData.type),
                        Text(cardData.priority.toString(), style: kTextStyleCardLabel), // TODO: extract constant
                        Text(cardData.formattedDateCreated, style: kTextStyleCardLabel), // TODO: is it created date?
                      ],
                    ),
                    SizedBox(height: 10.0),
                    Text(cardData.title, style: kTextStyleCardTitle),
                    SizedBox(height: 5.0),
                    Text(
                      cardData.body,
                      style: kTextStyleCardBody,
                      maxLines: 4,
                    ),
                    SizedBox(height: 13.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(cardData.id, style: kTextStyleCardId),
                        Text(cardData.author, style: kTextStyleCardLabel)
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(height: 8.0),
        ],
      ),
    );
  }
}

class CardBanner extends StatelessWidget {
  final CardType type;

  CardBanner(this.type);

  final titles = <CardType, String>{CardType.Approval: 'OPT', CardType.Assignment: 'Assignment'};

  final colors = <CardType, Color>{
    CardType.Approval: kColorCardBannerOpt,
    CardType.Assignment: kColorCardBannerAssignment,
  };

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 19,
      alignment: Alignment.center,
      child: Padding(
        padding: const EdgeInsets.only(left: 4.0, right: 4.0),
        child: Text(titles[type], style: kLabelTextStyle),
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(4)),
        color: colors[type],
      ),
    );
  }
}

class SlidableCard extends StatelessWidget {
  final CardData cardData;
  final Key key;
  final bool showSlideActions;

  const SlidableCard({
    this.key,
    this.cardData,
    this.showSlideActions,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Slidable(
      key: key,
      child: GestureDetector(
        onTap: () {
          print('card tap');
          Navigator.push(
            context,
            MaterialPageRoute(builder: (_) {
              return BlocProvider.value(
                value: BlocProvider.of<FlowBloc>(context),
                child: Details(cardData: cardData),
              );
            }),
          );
        },
        child: Card(cardData),
      ),
      actionExtentRatio: 0.25,
      actionPane: SlidableBehindActionPane(),
      actions: <Widget>[
        if (showSlideActions) ...[
          IconSlideAction(
            onTap: () {
              // TODO: raise an issue on github
              // https://github.com/letsar/flutter_slidable/issues/31
              // https://github.com/letsar/flutter_slidable/commit/84bb9d1b98d6b8529bf75c1a6dcc9a22654eff53
              /*
            var state = Slidable.of(context);
            state.dismiss();
            */
              print('approved');
              cardData.state = CardState.Approved;
              BlocProvider.of<FlowBloc>(context).add(FetchData());
            },
            iconWidget: SlideAction(
              iconData: FontAwesomeIcons.solidCheckCircle,
              color: kColorActionApprove,
            ),
          ),
        ],
      ],
      secondaryActions: <Widget>[
        if (showSlideActions) ...[
          IconSlideAction(
            onTap: () {
              print('rejected');
              cardData.state = CardState.Rejected;
              BlocProvider.of<FlowBloc>(context).add(FetchData());
            },
            iconWidget: SlideAction(
              iconData: FontAwesomeIcons.solidTimesCircle,
              color: kColorActionReject,
            ),
          ),
        ],
      ],
    );
  }
}

class SlideAction extends StatelessWidget {
  final IconData iconData;
  final Color color;

  SlideAction({
    @required this.iconData,
    @required this.color,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.only(
        bottom: 8.0,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(6)),
        color: color,
      ),
      child: Icon(
        iconData,
        size: 46,
        color: Colors.white,
      ),
    );
  }
}
