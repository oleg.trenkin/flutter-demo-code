import 'package:flutter/cupertino.dart';
import 'package:mvp/model/data_model.dart';

class TypeSlider extends StatelessWidget {
  final CardType activeType;
  final ValueChanged<CardType> callBack;

  final Map<int, Widget> children = <int, Widget>{
    0: Text('Approvals'),
    1: Text('Assignments'),
  };

  TypeSlider({
    this.activeType,
    this.callBack,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 16.0,
        right: 16.0,
        top: 12.0,
        bottom: 12.0,
      ),
      child: CupertinoSlidingSegmentedControl(
        children: children,
        groupValue: activeType == CardType.Approval ? 0 : 1,
        onValueChanged: (i) {
          callBack(i == 0 ? CardType.Approval : CardType.Assignment);
        },
      ),
    );
  }
}
