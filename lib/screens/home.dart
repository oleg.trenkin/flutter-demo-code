import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mvp/bloc/flow_bloc.dart';

import "package:mvp/utils/constants.dart";
import 'package:mvp/components/card.dart';
import 'package:mvp/components/type_slider.dart';
import 'package:mvp/components/top_bar.dart';
import 'package:mvp/model/data_model.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: kStringTitle,
      home: SafeArea(
        child: Container(
          color: Colors.white,
          child: BlocProvider(
            create: (context) => FlowBloc()..add(Reset()),
            child: Home(),
          ),
        ),
      ),
    );
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  bool isUpdated;

  @override
  void initState() {
    super.initState();
    isUpdated = true;
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FlowBloc, FlowState>(
      builder: (context, state) {
        return CustomScrollView(
          physics: const ClampingScrollPhysics(),
          slivers: <Widget>[
            if (state is FlowApprovals) ...[
              SliverTopBar(CardType.Approval),
              SliverTypeSlider(CardType.Approval),
              SliverCardList(),
            ],
            if (state is FlowAssignments) ...[
              SliverTopBar(CardType.Assignment),
              SliverTypeSlider(CardType.Assignment),
              SliverCardList(),
            ]
          ],
        );
      },
    );
  }
}

Iterable<SlidableCard> makeCardWidgetList(List<CardData> cards, FlowState state) sync* {
  for (var card in cards) {
    yield SlidableCard(
      key: UniqueKey(),
      cardData: card,
      showSlideActions: (state is FlowApprovals),
    );
  }
}

List<SlidableCard> filterList(List<SlidableCard> list, FlowState state) {
  final ret = list.where((element) {
    // Hide approved/rejected/completed cards
    if (element.cardData.state != CardState.Initial) return false;

    if (state is FlowAssignments)
      return element.cardData.type == CardType.Assignment;
    else
      return true;
  }).toList();

  return ret;
}

class SliverCardList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FlowBloc, FlowState>(
      builder: (context, state) {
        return SliverPadding(
          padding: const EdgeInsets.symmetric(horizontal: 12.0),
          sliver: SliverList(
            delegate: SliverChildListDelegate(
              filterList(makeCardWidgetList(CardListModel.list, state).toList(), state),
            ),
          ),
        );
      },
    );
  }
}

class SliverTypeSlider extends StatelessWidget {
  final CardType _activeType;

  const SliverTypeSlider(
    this._activeType,
  );

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      stretch: false,
      backgroundColor: Colors.white,
      floating: false,
      pinned: true,
      snap: false,
      flexibleSpace: TypeSlider(
        activeType: _activeType,
        callBack: (newType) {
          if (newType == CardType.Approval)
            BlocProvider.of<FlowBloc>(context).add(ShowApprovals());
          else
            BlocProvider.of<FlowBloc>(context).add(ShowAssignments());
        },
      ),
    );
  }
}

class SliverTopBar extends StatelessWidget {
  final CardType _activeType;

  SliverTopBar(
    this._activeType,
  );

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      stretch: true,
      expandedHeight: 170.0,
      backgroundColor: kColorTopBarBackground,
      floating: false,
      pinned: false,
      snap: false,
      actions: <Widget>[
        IconButton(
          icon: const Icon(Icons.settings, size: 26),
          tooltip: 'Demo: Reset State',
          onPressed: () {
            BlocProvider.of<FlowBloc>(context).add(Reset());
          },
        ),
      ],
      flexibleSpace: FlexibleSpaceBar(
        background: TopBar(
          approvalsTotal: CardListModel.approvalsCount,
          assignmentsTotal: CardListModel.assignmentsCount,
          activeType: _activeType,
        ),
      ),
    );
  }
}
