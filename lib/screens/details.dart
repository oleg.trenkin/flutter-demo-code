import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:mvp/model/data_model.dart';
import 'package:mvp/utils/constants.dart';
import 'package:mvp/bloc/flow_bloc.dart';

class Details extends StatelessWidget {
  final CardData cardData;

  const Details({Key key, this.cardData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: kColorTopBarBackground,
          flexibleSpace: Center(child: Text(cardData.id, style: kTextStyleCardTitle.copyWith(color: Colors.white))),
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(16.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TopInfo(cardData: cardData),
                SizedBox(height: 22),
                Text(cardData.title, style: kTextStyleCardTitleBold),
                SizedBox(height: 17),
                Text(cardData.body, style: kTextStyleCardBody),
                SizedBox(height: 22),
                InfoBlock(cardData: cardData),
                SizedBox(height: 22),
                Text('Assignments', style: kTextStyleCardTitleBold),
                SizedBox(height: 17),
                ASSIGNMENTS(), // TODO: make real
                SizedBox(height: 22),
                Text('Work notes', style: kTextStyleCardTitleBold),
                SizedBox(height: 17),
                WORKNOTES(), // TODO: make real
                SizedBox(height: 22),
                _buildActionsBlock(context),
              ],
            ),
          ),
        ),
        // bottomNavigationBar: ActionsBlock(),
      ),
    );
  }

  Column _buildActionsBlock(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        if (cardData.type == CardType.Approval) ...[
          GestureDetector(
            child: ActionButton('APPROVE', kColorActionApprove),
            onTap: () {
              cardData.state = CardState.Approved;
              BlocProvider.of<FlowBloc>(context).add(FetchData());
              Navigator.pop(context);
            },
          ),
          SizedBox(height: 10),
          Text('~or~', style: kTextStyleCardTitleBold),
          SizedBox(height: 10),
          Container(
              height: 68,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(1)),
                  border: Border.all(color: const Color(0xff979797), width: 1),
                  color: const Color(0x1ad8d8d8))),
          SizedBox(height: 10),
          GestureDetector(
            child: ActionButton('REJECT', kColorActionReject),
            onTap: () {
              cardData.state = CardState.Rejected;
              BlocProvider.of<FlowBloc>(context).add(FetchData());
              Navigator.pop(context);
            },
          ),
        ],
        if (cardData.type == CardType.Assignment) ...[
          GestureDetector(
            child: ActionButton('COMPLETE', kColorActionComplete),
            onTap: () {
              cardData.state = CardState.Completed;
              BlocProvider.of<FlowBloc>(context).add(FetchData());
              Navigator.pop(context);
            },
          ),
        ]
      ],
    );
  }
}

class InfoBlock extends StatelessWidget {
  final CardData cardData;
  const InfoBlock({
    Key key,
    @required this.cardData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Expanded(
          child: Container(
            alignment: Alignment.center,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Info(label: 'Tasker Originator', text: cardData.author),
                SizedBox(height: 23),
                Info(label: 'Tasker Manager', text: cardData.manager),
              ],
            ),
          ),
        ),
        Container(
            width: 1,
            height: 45,
            decoration: BoxDecoration(border: Border.all(color: const Color(0xff979797), width: 1))),
        Expanded(
          child: Container(
            alignment: Alignment.center,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Info(label: 'Created', text: cardData.formattedDateCreated),
                SizedBox(height: 23),
                Info(label: 'Completed', text: cardData.formattedDateCompleted),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class WorkNote extends StatelessWidget {
  const WorkNote({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Container(
                width: 2,
                height: 36,
                decoration: BoxDecoration(border: Border.all(color: const Color(0xfff9bc84), width: 2))),
            Expanded(
              child: Container(
                height: 36,
                decoration: BoxDecoration(
                  color: const Color(0x99f9ece0),
                ),
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(8.0, 4.0, 8.0, 4.0),
                  // TODO: extract
                  child: Text(
                    'Cras diam ipsum, facilisis eget tempor vel, ultricies quis nunc. Aliquam bibendum lorem et risus posuere.',
                    style: kTextStyleInfo,
                  ),
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 3.0),
        Container(
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.only(left: 8.0),
          child: Text(
            'Andrew Pishchulin 2020-04-28 11:27', // TODO: extract
            style: kTextStyleCardLabel,
          ),
        ),
      ],
    );
  }
}

class ASSIGNMENTS extends StatelessWidget {
  const ASSIGNMENTS({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        AssignmentBox(),
        SizedBox(height: 11),
        AssignmentBox(),
        SizedBox(height: 11),
        AssignmentBox(),
        SizedBox(height: 11),
        AssignmentBox(),
      ],
    );
  }
}

class WORKNOTES extends StatelessWidget {
  const WORKNOTES({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        WorkNote(),
        SizedBox(height: 11),
        WorkNote(),
        SizedBox(height: 11),
        WorkNote(),
        SizedBox(height: 11),
        WorkNote(),
      ],
    );
  }
}

class AssignmentBox extends StatelessWidget {
  const AssignmentBox({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Container(
          width: 2,
          height: 95,
          decoration: BoxDecoration(
            border: Border.all(color: const Color(0xff5c7080), width: 2),
          ),
        ),
        Expanded(
          child: Container(
            height: 95,
            decoration: BoxDecoration(
              color: const Color(0x99e4e8fb), // TODO: return original color
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Stack(
                children: <Widget>[
                  Info(label: 'Number', text: 'TOP0000729.1'),
                  Positioned(
                    top: 0,
                    right: 20,
                    child: Info(label: 'Assignment Group', text: 'Executive Assistance'),
                  ),
                  Positioned(
                      bottom: 0,
                      left: 0,
                      right: 0,
                      child: Info(
                          label: 'Description',
                          text:
                              'Cras diam ipsum, facilisis eget tempor vel, ultricies quis nunc. Aliquam bibendum lorem et risus posuere consectetur…')),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}

class ActionButton extends StatelessWidget {
  final String label;
  final Color backgroundColor;

  ActionButton(
    this.label,
    this.backgroundColor,
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: Text(label, style: kTextStyleButtonLabel),
      height: 46,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(2)),
        border: Border.all(color: const Color(0xff979797), width: 1),
        color: backgroundColor,
      ),
    );
  }
}

class TopInfo extends StatelessWidget {
  final CardData cardData;

  const TopInfo({Key key, @required this.cardData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Info(label: 'Bureau', text: 'A/IRM'), // TODO: fill from real model
        Info(label: 'Priority', text: cardData.formattedPriority),
        Info(label: 'Due date', text: cardData.formattedDateDue),
      ],
    );
  }
}

class Info extends StatelessWidget {
  final String label;
  final String text;

  Info({
    @required this.label,
    @required this.text,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(label, style: kTextStyleCardLabelSmall),
          SizedBox(height: 3.0),
          Text(
            text,
            style: kTextStyleInfoText,
            maxLines: 2,
          ),
        ],
      ),
    );
  }
}
