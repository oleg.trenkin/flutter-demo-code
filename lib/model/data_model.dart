import 'package:flutter/widgets.dart';
import 'package:mvp/utils/constants.dart';
import 'package:intl/intl.dart';
import 'dart:math' as math;

enum CardType {
  Approval,
  Assignment,
}

// TODO: "Medium (4-7 Days)" in spec is priority?
enum CardPriority {
  Medium,
}

const kCardPriorityTextMap = <CardPriority, String>{
  CardPriority.Medium: 'Medium (4-7 Days)',
};

enum CardState {
  Initial,
  Approved,
  Rejected,
  Completed,
}

class CardListModel {
  static var list = _init();

  static List<CardData> _init() => makeCardList(20).toList();
  static get initialList => list.where((element) => element.state == CardState.Initial).toList();
  static int get approvalsCount => initialList.where((element) => element.type == CardType.Approval).toList().length;
  static int get assignmentsCount => initialList.where((element) => element.type == CardType.Assignment).toList().length;

  static void reset() => list = _init();
}

class CardData {
  final String title;
  final String body;
  final String id;
  final String author;
  final String manager;
  final CardType type;
  final CardPriority priority;
  final DateTime dateCreated;
  final DateTime dateCompleted;
  final DateTime dateDue;

  CardState state = CardState.Initial;

  String get formattedDateCreated => DateFormat('yyyy-MM-dd').format(dateCreated);
  String get formattedDateCompleted => DateFormat('yyyy-MM-dd').format(dateCompleted);
  String get formattedDateDue => DateFormat('yyyy-MM-dd').format(dateDue);
  String get formattedPriority => kCardPriorityTextMap[priority]; 

  CardData({
    @required this.title,
    this.body,
    this.id,
    this.author,
    this.manager,
    this.type,
    this.priority,
    this.dateCreated,
    this.dateCompleted,
    this.dateDue,
  });
}

Iterable<CardData> makeCardList(int count) sync* {
  for (var i = 0; i < count; i++) {
    yield CardData(
      title: '$kStringCardTitle $i',
      body: kStringCardBody,
      id: 'TOP$i',
      author: 'John Doe',
      manager: 'Antonio Moreli',
      type: math.Random().nextInt(2) == 0 ? CardType.Approval : CardType.Assignment,
      priority: CardPriority.Medium,
      dateCreated: DateTime.now().subtract(Duration(days: 1)),
      dateCompleted: DateTime.now(),
      dateDue: DateTime.now().add(Duration(days: 7)),
    );
  }
}

// TODO: fill model from fake network data
/*
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

Future<List<CardItem>> fetchPosts(int startIndex, int limit) async {
  final response = await http.get('https://jsonplaceholder.typicode.com/posts?_start=$startIndex&_limit=$limit');
  if (response.statusCode == 200) {
    final data = json.decode(response.body) as List;
    return data.map((rawPost) {
      return CardItem(
        id: rawPost['id'],
        title: rawPost['title'],
        body: rawPost['body'],
      );
    }).toList();
  } else {
    throw Exception('error fetching posts');
  }
}
*/
