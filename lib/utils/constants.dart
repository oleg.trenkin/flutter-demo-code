import "package:flutter/material.dart";

// Strings
const kStringTitle = 'Flutter MVP App';
const kStringCardTitle = 'Executive memo';
const kStringCardBody =
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras a porta libero. Vestibulum libero lectus, sagittis ac odio eget, laoreet facilisis eros. Pellentesque vitae dignissim magna. Nam imperdiet euismod molestie. Mauris eu convallis justo. Cras diam ipsum, facilisis eget tempor vel, ultricies quis nunc. Aliquam bibendum lorem et risus posuere consectetur.';

// Colors
const kColorTopBarBackground = const Color(0xff394b59);
const kColorActionApprove = const Color(0xff0f9960);
const kColorActionReject = const Color(0xffdb3737);
const kColorActionComplete = Color(0xff137cbd);
const kColorCardBannerOpt = const Color(0xff357dfd);
const kColorCardBannerAssignment = const Color(0xffd9822b);

// Text styles
const kTextStyleCardTitle = const TextStyle(
  color: Colors.black,
  fontWeight: FontWeight.w400,
  fontFamily: 'Helvetica',
  fontSize: 18.0,
  letterSpacing: -0.43,
);

const kTextStyleCardBody = const TextStyle(
    color: const Color(0xff3a3f51),
    fontWeight: FontWeight.w400,
    fontFamily: 'Helvetica',
    fontSize: 14.0,
    letterSpacing: -0.34,
    height: 1.15 // TODO: experimental
    );

const kTextStyleCardId = const TextStyle(
  color: const Color(0xff106ba3),
  fontWeight: FontWeight.w300,
  fontFamily: 'Helvetica',
  fontSize: 12.0,
  letterSpacing: -0.29,
);

const kTextStyleCardLabel = const TextStyle(
  color: const Color(0xff5c7080),
  fontWeight: FontWeight.w300,
  fontFamily: 'Helvetica',
  fontSize: 11.0,
  letterSpacing: -0.26,
);

const kLabelTextStyle = const TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.w300,
  fontFamily: 'Helvetica',
  fontSize: 11.0,
  letterSpacing: -0.26,
);

const kTextStyleButtonLabel = const TextStyle(
  color: const Color(0xfffdfdfd),
  fontWeight: FontWeight.w700,
  fontFamily: "Helvetica",
  fontSize: 18.0,
);

const kCountTextStyle = const TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.w700,
  fontFamily: "Helvetica",
  fontSize: 76.0,
);

const kTopLabelTextStyle = const TextStyle(
  color: Colors.white,
  fontWeight: FontWeight.w400,
  fontFamily: "Helvetica",
  fontSize: 18.0,
);

const kBottomTextStyle = const TextStyle(
  color: const Color.fromRGBO(0, 0xff, 7, 0.7),
  fontWeight: FontWeight.w400,
  fontFamily: "Helvetica",
  fontSize: 16.0,
);

final kTextStyleCardTitleBold = kTextStyleCardTitle.copyWith(
  fontWeight: FontWeight.w700,
);

final kTextStyleCardLabelSmall = kTextStyleCardLabel.copyWith(
  fontSize: 11.0,
);

final kTextStyleInfoText = kTextStyleCardTitle.copyWith(
  fontSize: 14.0,
  letterSpacing: -0.34,
);

final kTextStyleInfo = kTextStyleCardBody.copyWith(
  fontSize: 12,
  letterSpacing: -0.29,
);
