part of 'flow_bloc.dart';

abstract class FlowState extends Equatable {
  const FlowState();
}

class FlowInitial extends FlowState {
  @override
  List<Object> get props => [];
}

class FlowApprovals extends FlowState {
  @override
  List<Object> get props => [];
}

class FlowAssignments extends FlowState {
  @override
  List<Object> get props => [];
}

class FlowUpdated extends FlowState {
  @override
  List<Object> get props => [];
}