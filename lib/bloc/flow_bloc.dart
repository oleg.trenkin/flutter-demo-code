import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import 'package:mvp/model/data_model.dart';

part 'flow_event.dart';
part 'flow_state.dart';

class FlowBloc extends Bloc<FlowEvent, FlowState> {
  @override
  FlowState get initialState => FlowInitial();

  @override
  Stream<FlowState> mapEventToState(
    FlowEvent event,
  ) async* {
    final oldState = state;

    if (event is Reset) {
      CardListModel.reset();
      yield FlowInitial();
      add(ShowApprovals());
    }

    if (event is ShowApprovals) {
      yield FlowApprovals();
    } else if (event is ShowAssignments) {
      yield FlowAssignments();
    } else if (event is FetchData) {
      yield FlowUpdated();
      if (oldState is FlowApprovals)
        add(ShowApprovals());
      else
        add(ShowAssignments());
    }
  }
}
