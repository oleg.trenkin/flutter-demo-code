part of 'flow_bloc.dart';

abstract class FlowEvent extends Equatable {
  const FlowEvent();
  @override
  List<Object> get props => [];
}

class Reset extends FlowEvent {}

class ShowApprovals extends FlowEvent {}

class ShowAssignments extends FlowEvent {}

class FetchData extends FlowEvent {}
